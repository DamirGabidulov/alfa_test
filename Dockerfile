#ставим версию слим
FROM gradle:5.3-jdk11-slim AS build

COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src

# второй шаг, для запуска, причем используем только jre для запуска (так легче)
FROM openjdk:11.0.7-jre-slim

RUN mkdir ./app

# копируем только джарник
COPY --from=build /home/gradle/src/build/libs/alfa_test-0.0.1-SNAPSHOT.jar /alfa.jar

# выполняет команду по запуску джарника
CMD ["java", "-jar", "/alfa.jar"]