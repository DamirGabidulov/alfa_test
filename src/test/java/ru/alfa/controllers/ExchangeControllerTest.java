package ru.alfa.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.alfa.models.Data;
import ru.alfa.models.Exchange;
import ru.alfa.models.Image;
import ru.alfa.services.impl.ExchangeServiceImpl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ExchangeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ExchangeServiceImpl exchangeService;

    Map<String, Double> rate = new HashMap<>();
    {
        rate.put("RUB", 61.0);
        rate.put("AUD", 1.39);
    }

    Exchange exchange = Exchange.builder()
            .disclaimer("Usage subject to terms: https://openexchangerates.org/terms")
            .license("https://openexchangerates.org/license")
            .timestamp(1654430427)
            .base("USD")
            .rates(rate)
            .build();

    Exchange exchangeRub = Exchange.builder()
            .rates(Collections.singletonMap("RUB", 61.0))
            .build();


    @BeforeEach
    public void setUp(){
        when(exchangeService.getLastRates()).thenReturn(exchange);
        when(exchangeService.getLastRatesByCurrency("RUB")).thenReturn(exchangeRub);
    }

    @Test
    void getLastRates() throws Exception {
        mockMvc.perform(get("/api/last"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.disclaimer", is("Usage subject to terms: https://openexchangerates.org/terms")))
                .andExpect(jsonPath("$.license", is("https://openexchangerates.org/license")))
                .andExpect(jsonPath("$.timestamp", is(1654430427)))
                .andExpect(jsonPath("$.base", is("USD")))
                .andExpect(jsonPath("$.rates", is(rate)));
    }

    @Test
    void getLastRatesByCurrency() throws Exception{
        mockMvc.perform(get("/api/last/RUB"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.rates.RUB").value(61.0));
    }


}