package ru.alfa.controllers;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ru.alfa.models.Data;
import ru.alfa.models.Gif;
import ru.alfa.models.Image;
import ru.alfa.services.impl.GifServiceImpl;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class GifControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GifServiceImpl gifService;

    Gif gif = Gif.builder()
            .data(Data.builder()
                    .type("gif")
                    .rating("g")
                    .url("shorUrl")
                    .images(Image.builder()
                            .original(Collections.singletonMap("url", "originalUrl"))
                            .build())
                    .build())
            .build();

    @BeforeEach
    void init(){
        when(gifService.getGif(anyString())).thenReturn(gif);
    }

    @Test
    void getGif() throws Exception {
        mockMvc.perform(get("/api/gif/ANYCODE"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.images.original.url").value("originalUrl"));
    }
}