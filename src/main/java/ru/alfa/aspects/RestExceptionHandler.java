package ru.alfa.aspects;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler (IllegalArgumentException.class)
    public ResponseEntity<Object> handleEntityNotFound(IllegalArgumentException exception){
        return ResponseEntity.status(404).body(exception.getMessage());
    }
}
