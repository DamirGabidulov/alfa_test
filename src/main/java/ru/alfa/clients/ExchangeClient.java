package ru.alfa.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import ru.alfa.models.Exchange;

@FeignClient(name = "ExchangeClient", url = "${openexchangerates.url}")
public interface ExchangeClient {

    @GetMapping("/latest.json")
    Exchange getLastRates(@RequestParam("app_id") String appId);

    @GetMapping("/latest.json")
    Exchange getLastRatesByCurrency(@RequestParam("app_id") String appId, @RequestParam("symbols") String currency);

    @GetMapping("/historical/{date}.json")
    Exchange getHistoricalRates(@PathVariable String date, @RequestParam("app_id") String appId);
}
