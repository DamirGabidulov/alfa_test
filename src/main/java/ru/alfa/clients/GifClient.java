package ru.alfa.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.alfa.models.Gif;

@FeignClient(name = "GifClient", url = "${giphy.com.url}")
public interface GifClient {

    @GetMapping("/random")
    Gif getGif(@RequestParam("api_key") String apiKey, @RequestParam("tag") String tag, @RequestParam("rating") String rating);
}
