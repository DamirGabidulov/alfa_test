package ru.alfa.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.alfa.models.Exchange;
import ru.alfa.services.ExchangeService;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ExchangeController {

    private final ExchangeService exchangeService;

    @GetMapping("/last")
    public Exchange getLastRates(){
        return exchangeService.getLastRates();
    }

    @GetMapping("/last/{currency}")
    public Exchange getLastRates(@PathVariable("currency") String currency){
        return exchangeService.getLastRatesByCurrency(currency);
    }

    @GetMapping("/historical")
    public Exchange getHistorical(){
        return exchangeService.getHistoricalRates();
    }

    @GetMapping("/compare/{currency}")
    public ResponseEntity<String> compare(@PathVariable("currency") String currency){
        return ResponseEntity.ok(exchangeService.compare(currency));
    }


}
