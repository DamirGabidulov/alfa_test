package ru.alfa.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.alfa.models.Gif;
import ru.alfa.services.GifService;

@RestController
@RequestMapping("/api/gif")
@RequiredArgsConstructor
public class GifController {

    private final GifService gifService;

    @GetMapping("/{currency}")
    public Gif getGif(@PathVariable("currency") String currency){
        return gifService.getGif(currency);
    }
}
