package ru.alfa.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Exchange {
    public String disclaimer;
    public String license;
    public Integer timestamp;
    public String base;
    public Map<String, Double> rates;
}
