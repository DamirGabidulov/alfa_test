package ru.alfa.services;

import ru.alfa.models.Exchange;

public interface ExchangeService {

    Exchange getLastRatesByCurrency(String currency);

    Exchange getLastRates();

    Exchange getHistoricalRates();

    String compare(String currency);
}
