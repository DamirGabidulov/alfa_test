package ru.alfa.services;

import ru.alfa.models.Gif;

public interface GifService {
    Gif getGif(String currency);

}
