package ru.alfa.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.alfa.clients.GifClient;
import ru.alfa.models.Gif;
import ru.alfa.services.ExchangeService;
import ru.alfa.services.GifService;

@Service
public class GifServiceImpl implements GifService {
    private GifClient gifClient;

    private final ExchangeService exchangeService;

    @Value("${giphy.com.api.key}")
    private String apiKey;
    @Value("${giphy.com.rating}")
    private String rating;
    @Autowired
    public GifServiceImpl(GifClient gifClient, ExchangeService exchangeService) {
        this.gifClient = gifClient;
        this.exchangeService = exchangeService;
    }

    @Override
    public Gif getGif(String currency) {
        String compareResult = exchangeService.compare(currency);
        if (compareResult.startsWith("Ошибка!")){
            throw new IllegalArgumentException(compareResult);
        }
        return gifClient.getGif(apiKey, compareResult, rating);
    }
}
