package ru.alfa.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.alfa.models.Exchange;
import ru.alfa.clients.ExchangeClient;
import ru.alfa.services.ExchangeService;

import java.time.LocalDate;
import java.util.Set;

@Service
public class ExchangeServiceImpl implements ExchangeService {

    private Exchange exchange;
    private ExchangeClient exchangeClient;

    @Value("${openexchangerates.app.id}")
    private String appId;

    @Value("${giphy.com.high.rate.tag}")
    private String highRateTag;

    @Value("${giphy.com.low.rate.tag}")
    private String lowRateTag;

    @Value("${giphy.com.same.rate.tag}")
    private String sameRateTag;


    @Autowired
    public ExchangeServiceImpl(ExchangeClient exchangeClient) {
        this.exchangeClient = exchangeClient;
    }


    @Override
    public Exchange getLastRates() {
        this.exchange = exchangeClient.getLastRates(appId);
        return exchange;
    }


    @Override
    public Exchange getLastRatesByCurrency(String currency) {
        Exchange currentExchange = exchangeClient.getLastRatesByCurrency(appId, currency);
        return currentExchange;
    }

    @Override
    public Exchange getHistoricalRates() {
        String yesterday = LocalDate.now().minusDays(1).toString();
        Exchange yesterdayExchange = exchangeClient.getHistoricalRates(yesterday, appId);

        return yesterdayExchange;
    }

    @Override
    public String compare(String currency) {
        Exchange currentExchange = getLastRates();
        //проверка кода валюты
        if (isCorrectCurrency(currentExchange, currency)) {
            Exchange yesterdayExchange = getHistoricalRates();

            int compare = Double.compare(yesterdayExchange.getRates().get(currency.toUpperCase()), currentExchange.getRates().get(currency.toUpperCase()));
            switch (compare) {
                case 1:
                    return lowRateTag;
                case 0:
                    return sameRateTag;
                case -1:
                    return highRateTag;
            }
            return null;
        } else {
            Set<String> currencyCodes = currentExchange.getRates().keySet();
            return "Ошибка! Пожалуйства выберите код валюты из " + currencyCodes;
        }
    }

    private boolean isCorrectCurrency(Exchange exchange, String currency) {
        return exchange.getRates().containsKey(currency.toUpperCase()) ? true : false;
    }
}
